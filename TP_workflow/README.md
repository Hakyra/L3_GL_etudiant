# Tictactoe

Implémentation C++/JavaScript du jeu de Tictactoe.

## Compilation du projet C++

Projet CMake classique, dans le dossier `cpp`.

## Compilation du projet JavaScript

Rester dans le dossier principal (qui contient ce `README.md`), puis :

```
docker run --rm -v `pwd`:/tmp -p3000:3000 -e USER_ID=`id -u` juliendehos/emscripten make -C js
firefox js/index.html
```

